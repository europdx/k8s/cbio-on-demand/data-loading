#!/bin/sh
find $PWD/core/src/main/scripts/ -type f -executable \! -name '*.pl'  -print0 | xargs -0 -- ln -st /usr/local/bin

sed -i "s/cbiodb/${DBHOST}/g" /cbioportal/portal.properties &&
sed -i "s/cbiodb/$DBHOST/g" /usr/local/tomcat/conf/context.xml

mv $CATALINA_HOME/webapps/cbioportal.war $CATALINA_HOME/webapps/${MOVE}.war
sh $CATALINA_HOME/bin/catalina.sh run
