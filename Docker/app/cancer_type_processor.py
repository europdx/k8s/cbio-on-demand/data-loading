from apihandler import ApiHandler

import conf

APILOCATION = conf.APPLICATION
STUDY = conf.STUDY
CANCERTYPES = conf.CANCERTYPES


class CancerType:
    """
    This class process cancer_type data type and write them to file structure defined by cBio
    """
    def __init__(self, metadata, study):
        self.metadata = metadata
        self.study = study

    def process(self):
        cancer_type = ApiHandler.call(APILOCATION + STUDY + '/' + self.study['name'] + CANCERTYPES, 'cancer type')
        self.write_cancer_type(cancer_type)
        self.write_cancer_type_metadata()

    def write_cancer_type_metadata(self):
        with open(self.study['name'] + '/' + 'meta_' + self.metadata['data_filename'], 'w') as output:
            output.write('genetic_alteration_type: ' + self.metadata['genetic_alteration_type'] + '\n')
            output.write('datatype: ' + self.metadata['datatype'] + '\n')
            output.write('data_filename: ' + self.metadata['data_filename'] + '\n')

    def write_cancer_type(self, cancer_type):
        with open(self.study['name'] + '/' + self.metadata['data_filename'], 'w') as output:
            output.write(cancer_type['oncotree_code'] + '\t' +
                         cancer_type['name'] + '\t' +
                         cancer_type['description'] + '\t' +
                         cancer_type['dedicated_color'] + '\t' +
                         cancer_type['parent_type_of_cancer'] + '\n')
