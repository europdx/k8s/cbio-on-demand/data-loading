import os

VERSION = '/v1'
#DATAHUB = os.environ.get("DATAHUB", "datahub-beta")
DATAHUB = os.environ.get("DATAHUB", "datahub")
APPLICATION = 'http://' + DATAHUB + '.edirex.ics.muni.cz:5000/api' + VERSION
STUDY = '/studies'
SAMPLES = '/samples'
TMP = '/tmplists'
PDXMODELS = '/pdxmodels'
METAFILES = '/metafiles'
CANCERTYPES = '/cancertypes'
CNAS = '/cnas'
MUT = '/mutations'
EXP = '/expressions'
LOGNAME = 'log.txt'
RETRIES = 3

CBIO = "cbio"
SSWORD = "P@ssword1"
DB_Config= "cbioportal"
PORT = 3306
CON_TIMEOUT = 10


#ID = os.environ.get('ID', 43)
#HOST = os.environ.get('DBHOST', 'test')
#DB_INTERFACE = None

# END