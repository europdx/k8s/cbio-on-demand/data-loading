import os

from apihandler import ApiHandler
import logging

VERSION = '/v1'
DATAHUB = os.environ.get("DATAHUB", "datahub-dev")
APPLICATION = 'http://' + DATAHUB + '.edirex.ics.muni.cz:5000/api' + VERSION
SAMPLES = '/samples'
TMP = '/tmplists'
# END
ID = os.environ.get('ID', 2)


class Sample:
    def __init__(self, metadata, study, models):
        self.metadata = metadata
        self.study = study
        self.models = models

    def write_metadata(self):
        with open(self.study['name'] + '/' + 'meta_' + self.metadata['data_filename'], 'w') as output:
            output.write('cancer_study_identifier: ' + self.study['cancer_study_identifier'] + '\n')
            output.write('genetic_alteration_type: ' + self.metadata['genetic_alteration_type'] + '\n')
            output.write('datatype: ' + self.metadata['datatype'] + '\n')
            output.write('data_filename: ' + self.metadata['data_filename'] + '\n')

    def write_header_samples(self, output):
        output.write("#Patient_Identifier\t"
                     "Sample Identifier\t"
                     "DATE_OF_COLLECTION\t"
                     "AGE_AT_COLLECTION\t"
                     "SAMPLE_ORIGIN\t"
                     "SITE_OF_SAMPLED_METASTASIS\t"
                     "IMPLANT_SITE\t"
                     "IMPLANT_TYPE\t"
                     "STRAIN\n")
        output.write(
            "#Identifier to uniquely specify a patient.\t"
            "A unique sample identifier.\t"
            "DATE OF COLLECTION (yyyy-mm-dd)\t"
            "\"Age at collection, Years (for UNITO is ""Age at diagnosis"", can be updated if needed)\"\t"
            "SAMPLE ORIGIN (Primary, Local Relapse, Metastasis, NA)\t"
            "SITE OF SAMPLED METASTASIS (Organ, NA)\t"
            "PDX IMPLANT SITE (Subcutis, Orthotopic, Both, NA)\t"
            "IMPLANT TYPE Tissue Fragment / Single Cell Suspension (FR, SCS)\t"
            "MOUSE STRAIN used for engraftment\n")
        output.write("#STRING\tSTRING\tSTRING\tNUMBER\tSTRING\tSTRING\tSTRING\tSTRING\tSTRING\n")
        output.write("#1\t1\t1\t2\t1\t1\t2\t1\t1\n")
        output.write("PATIENT_ID\tSAMPLE_ID\tDATE_OF_COLLECTION\tAGE_AT_COLLECTION\tSAMPLE_ORIGIN"
                     "\tSITE_OF_SAMPLED_METASTASIS\tIMPLANT_SITE\tIMPLANT_TYPE\tSTRAIN\n")

    def write_samples(self, output):
        all_samples = ApiHandler.call(APPLICATION + TMP + "/" + str(ID) + SAMPLES,
                                      "Retrieving samples for tmplist " + str(ID))
        for model in self.models:
            model_id = model["model_id"]
            sample_info = model['date_of_collection'] + '\t' + \
                          (model['age_of_collection'] if model['age_of_collection'] != 'Not Specified' else 'NA') + \
                          '\t' + (model['sample_origin'] or '') + '\t' + model['site_of_primary'] + '\t' + \
                          model['engraftment_site'] + '\t' + model['engraftment_type'] + '\t' + model['strain']
            if model_id in all_samples:
                for sample in all_samples[model_id]['samples']:
                    output.write(all_samples[model_id]["patient"] + '\t' + sample + '\t ' + sample_info + "\n")
            else:
                output.write(model['patient_id'] + '\t' + model_id + '\t ' + sample_info + "\n")

    def process(self):
        logging.debug("Writing Samples files")
        with open(self.study['name'] + '/' + self.metadata['data_filename'], 'w') as output:
            self.write_header_samples(output)
            self.write_samples(output)
        logging.debug("Writing Samples done")

        logging.debug("Writing metadata")
        self.write_metadata()
        logging.debug("Samples done")
