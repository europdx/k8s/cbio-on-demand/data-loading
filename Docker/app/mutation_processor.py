from apihandler import ApiHandler
import conf

APPLICATION = conf.APPLICATION
STUDY = conf.STUDY
TMP = conf.TMP
SAMPLES = conf.SAMPLES
PDXMODELS = conf.PDXMODELS
CNAS = conf.CNAS
EXP = conf.EXP
MUT = conf.MUT
CANCERTYPES = conf.CANCERTYPES
METAFILES = conf.METAFILES


class Mutation:
    """
    This class process mutation data type and write them to file structure defined by cBbio
    """
    def __init__(self, metadata, study, tmp_id_study, ids):
        self.metadata = metadata
        self.study = study
        self.tmp_id_study = tmp_id_study
        self.ids = ids

    def process(self):
        muts = ApiHandler.call(APPLICATION + TMP + '/' + self.tmp_id_study + MUT,
                                'mutation for tmplist: ' + self.tmp_id_study)

        if len(muts) == 0:
            return False

        with open(self.study['name'] + '/' + self.metadata['data_filename'], 'w') as output:
            self.write_header(output)

            self.write_mut(muts, output)

        self.write_metadata()
        return True

    def write_metadata(self):
        with open(self.study['name'] + '/meta_' + self.metadata['data_filename'], 'w') as output:
            output.write('cancer_study_identifier: ' + self.study['cancer_study_identifier'] + '\n')
            output.write('genetic_alteration_type: ' + self.metadata['genetic_alteration_type'] + '\n')
            output.write('datatype: ' + self.metadata['datatype'] + '\n')
            output.write('stable_id: ' + self.metadata['stable_id'] + '\n')
            output.write('show_profile_in_analysis_tab: ' + self.metadata['show_profile_in_analysis_tab'] + '\n')
            output.write('profile_description: ' + self.metadata['profile_description'] + '\n')
            output.write('profile_name: ' + self.metadata['profile_name'] + '\n')
            output.write('swissprot_identifier: ' + self.metadata['swissprot_identifier'] + '\n')
            output.write('data_filename: ' + self.metadata['data_filename'] + '\n')

    def write_mut(self, muts, output):
        delim = '\t'
        hugo = 'hugo_symbol'
        entrez = 'entrez_gene_id'

        for model in muts:
            for sample in model["mutations"]:
                self.ids.append(sample['sample_id'])
                for mut in sample['mutations']:
                    output.write(str(mut[hugo]) + delim)
                    output.write(str(mut[entrez]) + delim)
                    output.write(str(mut['ncbiBuild'])+ delim)
                    output.write(str(mut['chromosome']) + delim)
                    output.write(str(mut['start_position']) + delim)
                    output.write(str(mut['end_position'] if mut['end_position'] is not None else 'NA') + delim)
                    output.write(str(mut['strand']) + delim)
                    output.write(str(sample['sample_id']) + delim)
                    output.write(str(mut['variant_classification']) + delim)
                    output.write(str(mut['HGVSp_Short']) + delim)
                    output.write(str(mut['protein_position']) + delim)
                    output.write(str(mut['swissprot']) + '\n')

    def write_header(self, output):
        output.write('Hugo_Symbol	Entrez_Gene_Id	NCBI_Build	Chromosome	Start_Position'
                     '	End_Position	Strand	Tumor_Sample_Barcode	Variant_Classification'
                     '	HGVSp_Short	Protein_position	SWISSPROT' + '\n')

