from requests import api
import json as js

from apihandler import ApiHandler

VERSION = '/v1'
APILOCATION = 'http://datahub-dev.edirex.ics.muni.cz:5000/api' + VERSION
PDXMODELS = '/pdxmodels'
TMP = '/tmplists'

pdxmodels = ApiHandler.call(APILOCATION + PDXMODELS, 'pdxmodels test')
ids = []
for pdxmodel in pdxmodels:
    ids.append(pdxmodel['model_id'])



print(len(ids))
ids = list(set(ids))
print(len(ids))


response = api.post(APILOCATION + TMP, json={'pdxmodel_list': ids})
data = js.loads(response.content.decode('utf-8'))
print(str(data['tmplistid']))
