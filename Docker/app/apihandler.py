import logging
import time

from requests import api
import json as js


class ApiHandler:
    @staticmethod
    def call(url, log):
        """
        Do request to datahub for any data and return resources in json
        Log each call
        :param url: for api call
        :param log: what resource is being  called
        :return: data in json
        """
        logging.debug(' Retrieving ' + log)
        start = time.time()
        response = api.get(url)
        end = time.time()
        logging.info('Retrieving ' + log + ' in ' + str(end-start) + ' sec')
        if response.status_code != 200:
            logging.debug('Code:' + str(response.status_code) + 'were returned from ' + url + ' when retrieving ' + log)
        logging.debug(' Retrieved ' + log)
        returns = js.loads(response.content.decode('utf-8'))
        return returns
