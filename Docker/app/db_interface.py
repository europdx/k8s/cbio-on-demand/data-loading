class Database:
    def __init__(self, db):
        self.db = db

    def start_initialization(self):
        sql = 'CREATE TABLE IF NOT EXISTS import (' \
              'completed BOOLEAN DEFAULT 0' \
              ');'
        cur = self.db.cursor()
        cur.execute(sql)

    def is_imported(self):
        sql = 'SELECT * FROM import'
        cur = self.db.cursor()
        cur.execute(sql)
        result = cur.fetchall()
        if len(result) != 1:
            return False
        if result[0][0] == 1:
            return True
        return False

    def flag_imported(self):
        sql = 'INSERT INTO import (completed) VALUES (%s)'
        val = (1,)
        cur = self.db.cursor()
        cur.execute(sql, val)
        self.db.commit()


class Fake(Database):

    def __init__(self):
        super().__init__(None)

    def start_initialization(self):
        return

    def is_imported(self):
        return True

    def flag_imported(self):
        return
