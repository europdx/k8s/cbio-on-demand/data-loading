class CaseList:

    def __init__(self, study, stable_id, name, description, ids):
        self.identifier = study['cancer_study_identifier']
        self.stable_id = stable_id
        self.name = name
        self.description = description
        self.ids = ids
        self.study = study

    """
    Create case list based on class (CaseList) attributes
    """
    def process(self):
        ids_string = '\t'.join(self.ids)
        with open(self.study['name'] + '/case_lists' + '/cases_' + self.stable_id, 'w') as output:
            output.write('cancer_study_identifier: ' + self.identifier + '\n')
            output.write('stable_id: ' + self.identifier + '_' + self.stable_id + '\n')
            output.write('case_list_name: ' + self.name + '\n')
            output.write('case_list_description: ' + self.description + '\n')
            output.write('case_list_ids: ' + ids_string + '\n')
